package com.artem.flickrfinder.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.artem.flickrfinder.database.PhotoDatabase
import com.artem.flickrfinder.rest.Photo

/**
 * Bookmarks module is responsible for saving/deleting/retrieving the Bookmarks and associated photos
 */
class Bookmarks(private val db: PhotoDatabase, private val imageDownloader: ImageDownloader) {

    fun save(photo: Photo) {
        db.photoDao().insert(photo)
        imageDownloader.downloadImage(photo)
    }

    fun delete(photo: Photo) {
        db.photoDao().delete(photo)
        imageDownloader.deleteImage(photo)
    }

    fun isBookmarked(photo: Photo): Boolean {
        return db.photoDao().findById(photo.id).isNotEmpty()
    }

    fun getAllBookmarkedPhotos(): LiveData<List<Photo>> {
        //Mark photos as 'from database' to distinguish data source.
        return Transformations.map(db.photoDao().getAll()) { it.onEach { photo -> photo.fromDb = true } }
    }

    fun getDownloadedImage(photoId: String) = imageDownloader.getImage(photoId)
}
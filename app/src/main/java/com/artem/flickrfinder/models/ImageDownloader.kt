package com.artem.flickrfinder.models

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import com.artem.flickrfinder.rest.Photo
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.Executors

/**
 * Helper class that handles downloading images to disk cache / retrieving them
 */
class ImageDownloader(private val downloadFolder: File, private val picasso: Picasso) {
    private val executor = Executors.newCachedThreadPool()

    fun downloadImage(photo: Photo) {
        picasso
            .load(photo.urlHD)
            .into(getTarget(photo.id))
    }

    fun getImage(id: String): File? {
        val image = File(downloadFolder, id)
        return if (image.exists()) image else null
    }

    fun deleteImage(photo: Photo) {
        val image = File(downloadFolder, photo.id)
        if (image.exists()) image.delete()
    }

    private fun getTarget(id: String): Target {
        return object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                Log.e("ImageDownloader", "Exception while downloading photo: $e")
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                executor.submit {
                    val file = File(downloadFolder, id)
                    try {
                        file.createNewFile()
                        val stream = FileOutputStream(file)
                        bitmap?.compress(Bitmap.CompressFormat.JPEG, 80, stream)
                        stream.flush()
                        stream.close()
                    } catch (e: IOException) {
                        Log.e("ImageDownloader", "Exception while saving photo to disk: $e")
                    }
                }
            }
        }
    }
}
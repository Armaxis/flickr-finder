package com.artem.flickrfinder.models

import com.artem.flickrfinder.rest.FlickrAPI
import com.artem.flickrfinder.rest.PhotosSearchResult
import com.artem.flickrfinder.rest.SearchResponse
import retrofit2.Call
import retrofit2.Response

/**
 * Main networking component, allows to request photos from FlickAPI by pages.
 */
class FlickrConnector(private val api: FlickrAPI, private val apiKey: String) {

    fun search(query: String, page: Int = 1, callback: Callback) {
        api.photoSearch(
            query = query,
            apiKey = apiKey,
            pageSize = PAGE_SIZE,
            page = page
        ).enqueue(object : retrofit2.Callback<SearchResponse?> {
            override fun onFailure(call: Call<SearchResponse?>, t: Throwable) {
                callback.onError(t)
            }

            override fun onResponse(call: Call<SearchResponse?>, response: Response<SearchResponse?>) {
                val searchResult = if (response.successful()) response.body()?.photos else null
                if (searchResult != null) {
                    callback.onSuccess(searchResult)
                } else {
                    callback.onError(Exception("Request failed! Error code: ${response.code()}"))
                }
            }
        })
    }

    fun Response<SearchResponse?>.successful() = isSuccessful && body()?.stat == STATUS_OK

    interface Callback {
        fun onSuccess(photoList: PhotosSearchResult)
        fun onError(e: Throwable)
    }

    companion object {
        private const val STATUS_OK = "ok"
        private const val PAGE_SIZE = 25
    }
}

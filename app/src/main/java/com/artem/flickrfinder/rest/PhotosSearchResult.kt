package com.artem.flickrfinder.rest

data class PhotosSearchResult(
    val page: Int,
    val pages: Int,
    val perpage: Int,
    val photo: List<Photo>,
    val total: String
)
package com.artem.flickrfinder.rest

data class SearchResponse(
    val photos: PhotosSearchResult,
    val stat: String
)
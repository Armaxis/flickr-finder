package com.artem.flickrfinder.rest

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


interface FlickrAPI {

    @Headers("Content-Type: application/json")
    @GET("$PATH?method=$SEARCH_METHOD&$JSON_FORMAT_PARAM")
    fun photoSearch(
        @Query("text") query: String,
        @Query("api_key") apiKey: String,
        @Query("sort") sortOrder: String = SORT_INTERESTINGNESS_DESC,
        @Query("per_page") pageSize: Int = 25,
        @Query("page") page: Int = 1
    ): Call<SearchResponse>

    companion object {
        const val PATH = "services/rest/"
        const val JSON_FORMAT_PARAM = "format=json&nojsoncallback=1"
        const val SEARCH_METHOD = "flickr.photos.search"

        //Sorting types
        const val SORT_DATE_POSTED_ASC = "date-posted-asc"
        const val SORT_DATE_POSTED_DESC = "date-posted-desc"
        const val SORT_DATE_TAKEN_ASC = "date-taken-asc"
        const val SORT_DATE_TAKEN_DESC = "date-taken-desc"
        const val SORT_INTERESTINGNESS_DESC = "interestingness-desc"
        const val SORT_INTERESTINGNESS_ASC = "interestingness-asc"
        const val SORT_RELEVANCE = "relevance"
    }
}
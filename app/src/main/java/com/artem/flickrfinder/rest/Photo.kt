package com.artem.flickrfinder.rest

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Photo(
    val farm: Int,
    @PrimaryKey val id: String,
    val isfamily: Int,
    val isfriend: Int,
    val ispublic: Int,
    val owner: String,
    val secret: String,
    val server: String,
    val title: String,
    var fromDb: Boolean = false
) {
    val url get() = "http://farm$farm.staticflickr.com/$server/${id}_${secret}_z.jpg" //640 on longest side
    val urlHD get() = "http://farm$farm.staticflickr.com/$server/${id}_${secret}_b.jpg" //1024 on longest side
}
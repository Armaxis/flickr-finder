package com.artem.flickrfinder.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.artem.flickrfinder.FlickrFinderApplication
import com.artem.flickrfinder.R
import com.artem.flickrfinder.rest.Photo
import com.squareup.picasso.Callback
import com.squareup.picasso.RequestCreator

class PhotosListAdapter(private val listener: ClickListener) : RecyclerView.Adapter<PhotosListAdapter.ViewHolder>() {
    private val bookmarks = FlickrFinderApplication.getComponent().getBookmarks()
    private val picasso = FlickrFinderApplication.getComponent().getPicasso()

    var data: List<Photo>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.photo_view, parent, false) as CardView

        return ViewHolder(cardView)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = data?.get(position) ?: return
        holder.bind(photo)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(photo: Photo) {
            text.text = photo.title
            image.contentDescription = photo.title //Accessibility
            spinner.visibility = View.VISIBLE
            updateBookmarkIcon(photo.bookmarked())
            createImageRequest(photo)
                .placeholder(R.color.lightGrey)
                .error(R.drawable.image_error_placeholder)
                .into(image, object : Callback {
                    override fun onSuccess() = hideSpinner()
                    override fun onError(e: Exception?) = hideSpinner()
                })
            itemView.setOnClickListener { listener.onClicked(photo) }

            bookmark.setOnClickListener {
                //Update the bookmark status, refresh icon
                with(photo) {
                    val bookmarked = bookmarked()
                    when {
                        bookmarked -> bookmarks.delete(this)
                        else -> bookmarks.save(this)
                    }
                    updateBookmarkIcon(!bookmarked) //Invert the icon after the operation
                }
            }
        }

        private fun updateBookmarkIcon(bookmarked: Boolean) {
            bookmark.setImageResource(if (bookmarked) R.drawable.ic_star_yellow else R.drawable.ic_star_grey)
        }

        private fun hideSpinner() {
            spinner.visibility = View.GONE
        }

        private val text = view.findViewById<TextView>(R.id.text)
        private val image = view.findViewById<ImageView>(R.id.image)
        private val spinner = view.findViewById<ProgressBar>(R.id.loading_spinner)
        private val bookmark = view.findViewById<ImageView>(R.id.bookmark)
    }

    private fun createImageRequest(photo: Photo): RequestCreator {
        val file = if (photo.fromDb) bookmarks.getDownloadedImage(photo.id) else null
        return if (file != null) {
            picasso.load(file)
        } else {
            picasso.load(photo.url)
        }
    }

    private fun Photo.bookmarked() = bookmarks.isBookmarked(this)

    interface ClickListener {
        fun onClicked(photo: Photo)
    }
}
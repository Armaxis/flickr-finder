package com.artem.flickrfinder.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.artem.flickrfinder.rest.Photo

@Dao
interface PhotoDao {
    @Query("SELECT * FROM Photo")
    fun getAll(): LiveData<List<Photo>>

    @Query("SELECT * FROM Photo WHERE id = (:id) ")
    fun findById(id: String): List<Photo>

    @Insert
    fun insert(photo: Photo)

    @Delete
    fun delete(photo: Photo)
}
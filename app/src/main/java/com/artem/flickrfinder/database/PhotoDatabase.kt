package com.artem.flickrfinder.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.artem.flickrfinder.rest.Photo

@Database(entities = [Photo::class], version = 1)
abstract class PhotoDatabase : RoomDatabase() {
    abstract fun photoDao(): PhotoDao
}
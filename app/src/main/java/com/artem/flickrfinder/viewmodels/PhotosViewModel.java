package com.artem.flickrfinder.viewmodels;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.artem.flickrfinder.FlickrFinderApplication;
import com.artem.flickrfinder.models.FlickrConnector;
import com.artem.flickrfinder.rest.Photo;
import com.artem.flickrfinder.rest.PhotosSearchResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PhotosViewModel extends ViewModel {
    private MutableLiveData<List<Photo>> photos = new MutableLiveData<>();
    private MutableLiveData<String> error = new MutableLiveData<>();
    private String query;
    private int currentPage = 0;
    private int totalPages = Integer.MAX_VALUE;
    private boolean requestInProgress;

    public LiveData<List<Photo>> getPhotos() {
        return photos;
    }

    public LiveData<String> getError() {
        return error;
    }

    public String getQuery() {
        return query;
    }

    public void onItemVisible(int position) {
        //Callback when scrolling happens. Check if we need to preload next page
        //Ideally, it should be a Paging Library...
        List<Photo> photos = this.photos.getValue();
        if (!requestInProgress && photos != null && position > photos.size() - 10) {
            //Load next page
            loadPage();
        }
    }

    public void loadPhotos(@NonNull String query) {
        this.query = query;
        if (query.isEmpty()) {
            //Empty query -> make an empty list
            photos.setValue(Collections.emptyList());
        } else {
            loadPage();
        }
    }

    private void loadPage() {
        if (currentPage >= totalPages || requestInProgress) {
            //Either next page is already been requested or we reached the end
            return;
        }
        requestInProgress = true;
        //Load next page of data
        FlickrFinderApplication.getComponent().getFlickrConnector().search(query, currentPage + 1, new FlickrConnector.Callback() {
            @Override
            public void onSuccess(@NonNull PhotosSearchResult result) {
                totalPages = result.getPages();
                currentPage = result.getPage();
                List<Photo> currentList = photos.getValue();
                if (currentList == null) {
                    currentList = new ArrayList<>();
                }

                currentList.addAll(result.getPhoto());
                photos.postValue(currentList);
                requestInProgress = false;
            }

            @Override
            public void onError(@NonNull Throwable e) {
                error.postValue(e.getMessage());
                requestInProgress = false;
                //Current page stays unchanged, so request can be triggered again
            }
        });
    }
}

package com.artem.flickrfinder.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.artem.flickrfinder.FlickrFinderApplication;
import com.artem.flickrfinder.models.Bookmarks;
import com.artem.flickrfinder.rest.Photo;

import java.util.List;

public class BookmarksViewModel extends ViewModel {
    private LiveData<List<Photo>> photos = new MutableLiveData<>();
    private Bookmarks bookmarks = FlickrFinderApplication.getComponent().getBookmarks();

    public LiveData<List<Photo>> getPhotos() {
        if (photos.getValue() == null) {
            photos = bookmarks.getAllBookmarkedPhotos();
        }
        return photos;
    }
}

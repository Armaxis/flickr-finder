package com.artem.flickrfinder.di

import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File
import javax.inject.Named
import javax.inject.Singleton


@Module(includes = [ContextModule::class])
class OkHttpClientModule {

    @Singleton
    @Provides
    fun okHttpClient(cache: Cache) = OkHttpClient()
        .newBuilder()
        .cache(cache)
        .build()

    @Provides
    fun cache(@Named("okhttp_cache") cacheFile: File) = Cache(cacheFile, CACHE_SIZE)

    @Provides
    @Named("okhttp_cache")
    fun file(context: Context) = File(context.cacheDir, HTTP_CACHE_DIR).apply { mkdirs() }

    companion object {
        private const val CACHE_SIZE: Long = 1024 * 1024 * 40 //40MB cache
        private const val HTTP_CACHE_DIR: String = "HttpCache"
    }
}
package com.artem.flickrfinder.di

import android.content.Context
import androidx.room.Room
import com.artem.flickrfinder.database.PhotoDatabase
import com.artem.flickrfinder.models.Bookmarks
import com.artem.flickrfinder.models.ImageDownloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import java.io.File
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [PicassoModule::class])
class BookmarksModule {

    @Provides
    @Singleton
    fun bookmarks(db: PhotoDatabase, imageDownloader: ImageDownloader) = Bookmarks(db, imageDownloader)

    @Provides
    @Singleton
    fun db(context: Context) =
        Room.databaseBuilder(context, PhotoDatabase::class.java, DB_NAME)
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun imageDownloader(@Named("download_folder") downloadFolder: File, picasso: Picasso) = ImageDownloader(downloadFolder, picasso)

    @Provides
    @Named("download_folder")
    fun downloadFolder(context: Context) = File(context.cacheDir, DOWNLOAD_FOLDER_NAME).apply { mkdirs() }

    companion object {
        private const val DB_NAME = "photos-database"
        private const val DOWNLOAD_FOLDER_NAME = "downloaded"
    }
}
package com.artem.flickrfinder.di

import com.artem.flickrfinder.models.Bookmarks
import com.artem.flickrfinder.models.FlickrConnector
import com.squareup.picasso.Picasso
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [BookmarksModule::class, FlickrModule::class])
interface FlickrComponent {

    @Singleton
    fun getBookmarks(): Bookmarks

    @Singleton
    fun getPicasso(): Picasso

    @Singleton
    fun getFlickrConnector(): FlickrConnector
}
package com.artem.flickrfinder.di

import android.content.Context
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton


@Module(includes = [OkHttpClientModule::class])
class PicassoModule {

    @Singleton
    @Provides
    fun picasso(context: Context, okHttp3Downloader: OkHttp3Downloader) =
        Picasso.Builder(context).downloader(okHttp3Downloader).build()

    @Provides
    fun okHttp3Downloader(okHttpClient: OkHttpClient) = OkHttp3Downloader(okHttpClient)
}
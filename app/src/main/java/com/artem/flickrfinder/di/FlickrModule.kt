package com.artem.flickrfinder.di

import android.content.Context
import com.artem.flickrfinder.R
import com.artem.flickrfinder.models.FlickrConnector
import com.artem.flickrfinder.rest.FlickrAPI
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module(includes = [OkHttpClientModule::class])
class FlickrModule {

    @Provides
    fun flickrConnector(api: FlickrAPI, apiKey: String) = FlickrConnector(api, apiKey)

    @Provides
    fun flickApi(retrofit: Retrofit) = retrofit.create(FlickrAPI::class.java)

    @Provides
    fun apiKey(context: Context) = context.getString(R.string.flickr_api_key)

    @Singleton
    @Provides
    fun retrofit(okHttpClient: OkHttpClient, moshiConverterFactory: MoshiConverterFactory) =
        Retrofit.Builder()
            .baseUrl(FLICKR_URL)
            .client(okHttpClient)
            .addConverterFactory(moshiConverterFactory)
            .build()

    @Provides
    fun moshiConverterFactory(moshi: Moshi) = MoshiConverterFactory.create(moshi)

    @Provides
    fun moshi() = Moshi.Builder().build()

    companion object {
        private const val FLICKR_URL = "https://api.flickr.com"
    }
}
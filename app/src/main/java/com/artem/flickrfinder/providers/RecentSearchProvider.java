package com.artem.flickrfinder.providers;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Simple search suggestion provider that saves search queries
 */
public class RecentSearchProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.artem.flickrfinder.RecentSearchProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public RecentSearchProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}

package com.artem.flickrfinder.ui

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.navOptions
import androidx.navigation.ui.NavigationUI
import com.artem.flickrfinder.R
import com.artem.flickrfinder.providers.RecentSearchProvider
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        NavigationUI.setupWithNavController(toolbar, findNavController(R.id.nav_host_fragment));
        //Prevent UI from jumping when coming back from full-screen viewer
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        initSearchMenu(menu)

        return true
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        // Verify the action and get the query
        if (Intent.ACTION_SEARCH == intent?.action) {
            intent.getStringExtra(SearchManager.QUERY)?.let { query ->
                showSearchResults(query)
                saveSearchToRecents(query)
                hideSearchMenu()
            }
        }
    }

    private fun saveSearchToRecents(query: String) {
        SearchRecentSuggestions(this, RecentSearchProvider.AUTHORITY, RecentSearchProvider.MODE)
            .saveRecentQuery(query, null)
    }

    private fun showSearchResults(query: String) {
        val options = navOptions {
            anim {
                enter = android.R.anim.fade_in
                exit = android.R.anim.fade_out
                popEnter = android.R.anim.fade_out
                popExit = android.R.anim.fade_in
            }
            launchSingleTop = true
            popUpTo = R.id.search_results_dest
        }
        findNavController(R.id.nav_host_fragment).navigate(R.id.search_results_dest, Bundle().apply { putString("query", query) }, options)
    }

    //region Search menu manipulations
    private lateinit var searchView: SearchView
    private lateinit var searchMenuItem: MenuItem

    private fun initSearchMenu(menu: Menu) {
        //Some workarounds are required to make SearchView behave nicely, e.g. show keyboard, collapse when pressing 'Close'
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchMenuItem = menu.findItem(R.id.menu_search)
        searchView = searchMenuItem.actionView as SearchView
        searchView.apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnCloseListener {
                //When pressing 'X' to clear text input on already clear text - it doesn't collapse automatically
                hideSearchMenu()
                false
            }
        }

        searchMenuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                //Force the search view to the non-iconified state, so it will invoke the keyboard properly
                searchView.isIconified = false
                //Force the suggestions to pop up. Ugly.
                searchView.setQuery(".", false)
                searchView.setQuery("", false)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                //When menu is closed, the keyboard doesn't autohide.
                searchView.hideKeyboard()
                return true
            }
        })
    }

    private fun hideSearchMenu() {
        searchView.setQuery("", false)
        searchMenuItem.collapseActionView()
    }
    //endregion

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}

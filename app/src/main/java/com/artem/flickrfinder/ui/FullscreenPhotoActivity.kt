package com.artem.flickrfinder.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.artem.flickrfinder.FlickrFinderApplication
import com.artem.flickrfinder.R
import com.squareup.picasso.Callback
import kotlinx.android.synthetic.main.activity_fullscreen_photo.*


class FullscreenPhotoActivity : AppCompatActivity() {
    private val bookmarks = FlickrFinderApplication.getComponent().getBookmarks()
    private val picasso = FlickrFinderApplication.getComponent().getPicasso()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen_photo)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //All the data for this activity is stored inside the bundle, so on orientation change we will be receiving
        //the same bundle with data, so there's no need for a ViewModel for now.
        val id = intent.getStringExtra(PHOTO_ID)
        val url = intent.getStringExtra(PHOTO_URL)
        val title = intent.getStringExtra(PHOTO_TITLE)

        setTitle(title)
        hideUI()
        loadPhoto(id, url)

        photoView.setOnClickListener {
            //Tapping on photo should show/hide UI
            supportActionBar?.run {
                if (isShowing) hideUI() else showUI()
            }
        }
    }

    private fun loadPhoto(id: String?, url: String?) {
        showLoadingSpinner()

        //When provided ID - load from file, else fetch via network
        val imageRequest =
            when {
                id.isNullOrEmpty() -> picasso.load(url) //Url can be null, it will show placeholder
                else -> bookmarks.getDownloadedImage(id)?.let { picasso.load(it) }
            }

        if (imageRequest == null) {
            //Supposed to be loading from disk, but there's no file. Display error placeholder
            photoView.apply {
                setImageDrawable(getDrawable(R.drawable.image_error_placeholder))
                showUI()
                hideLoadingSpinner()
                isZoomable = false
            }
            message(R.string.full_screen_request_error)
            return
        }

        imageRequest
            .error(R.drawable.image_error_placeholder)
            .into(photoView, object : Callback {
                override fun onSuccess() {
                    if (shouldShowHint()) message(R.string.zoom_hint)
                    hideLoadingSpinner()
                }

                override fun onError(e: Exception?) {
                    message(R.string.full_screen_request_error)
                    hideLoadingSpinner()
                }
            })
    }

    private fun showLoadingSpinner() {
        loading_spinner.visibility = View.VISIBLE
    }

    private fun hideLoadingSpinner() {
        loading_spinner.visibility = View.GONE
    }

    private fun hideUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
            or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN)
        supportActionBar?.hide()
    }

    private fun showUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        supportActionBar?.show()
    }

    fun message(message: Int) = Toast.makeText(this, getString(message), Toast.LENGTH_SHORT).show()

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(0, 0)
    }

    companion object {
        private const val PHOTO_URL = "photo_url"
        private const val PHOTO_ID = "photo_id"
        private const val PHOTO_TITLE = "photo_title"

        //First three times user opens image in fullscreen we show a tip about pinch and zoom
        private var hintCount = 3

        fun shouldShowHint(): Boolean = hintCount-- > 0

        fun newInstanceWithId(context: Context, photoId: String, photoTitle: String) = Intent(context, FullscreenPhotoActivity::class.java).apply {
            putExtra(PHOTO_ID, photoId)
            putExtra(PHOTO_TITLE, photoTitle)
        }

        fun newInstanceWithUrl(context: Context, photoUrl: String, photoTitle: String) = Intent(context, FullscreenPhotoActivity::class.java).apply {
            putExtra(PHOTO_URL, photoUrl)
            putExtra(PHOTO_TITLE, photoTitle)
        }
    }
}
package com.artem.flickrfinder.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import com.artem.flickrfinder.R;
import com.artem.flickrfinder.rest.Photo;
import com.artem.flickrfinder.viewmodels.PhotosViewModel;

import java.util.List;

public class SearchResultsFragment extends PhotoListFragment {
    private String query;
    private PhotosViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            query = bundle.getString("query", "");
        }

        viewModel = ViewModelProviders.of(this).get(PhotosViewModel.class);
        if (viewModel.getQuery() == null) {
            //First usage, trigger photos request
            viewModel.loadPhotos(query);
        }

        return super.onCreateView(inflater, parent, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fab.setOnClickListener(v -> {
            Navigation.findNavController(getActivity(), R.id.nav_host_fragment).navigate(R.id.bookmarks_dest);
        });

        viewModel.getPhotos().observe(this, photos -> {
            if (photos.isEmpty()) {
                //No photos -> show welcoming text or empty text
                String query = viewModel.getQuery();
                displayText(TextUtils.isEmpty(query) ?
                    getString(R.string.welcome_text) : getString(R.string.nothing_found, query));
            } else {
                displayPhotos(photos);
            }
        });

        viewModel.getError().observe(this, errorText -> {
            if (adapter.getItemCount() != 0) {
                //Don't hide the list, but notify user about error:
                Toast.makeText(getContext(), getString(R.string.request_error, errorText), Toast.LENGTH_LONG).show();
                return;
            }
            if (!TextUtils.isEmpty(errorText)) {
                displayText(getString(R.string.request_error, errorText));
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                viewModel.onItemVisible(layoutManager.findFirstVisibleItemPosition());
            }
        });
    }

    private void displayText(String text) {
        emptyText.setVisibility(View.VISIBLE);

        emptyText.setText(text);
        recyclerView.setVisibility(View.GONE);
    }

    private void displayPhotos(List<Photo> photos) {
        emptyText.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter.setData(photos);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPhotoClicked(Photo photo) {
        Intent intent = FullscreenPhotoActivity.Companion.newInstanceWithUrl(getContext(), photo.getUrlHD(), photo.getTitle());
        startActivity(intent);
        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}

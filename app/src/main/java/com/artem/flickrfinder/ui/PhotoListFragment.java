package com.artem.flickrfinder.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.artem.flickrfinder.R;
import com.artem.flickrfinder.adapters.PhotosListAdapter;
import com.artem.flickrfinder.rest.Photo;

/**
 * Base class that has a list of photos and a textview for displaying messages
 */
abstract class PhotoListFragment extends Fragment {
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    PhotosListAdapter adapter;
    TextView emptyText;
    View fab;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_photo_list, parent, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.itemlist);
        emptyText = view.findViewById(R.id.emptyText);
        fab = view.findViewById(R.id.fab);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new PhotosListAdapter(this::onPhotoClicked);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new MarginItemDecoration(10));
    }

    protected abstract void onPhotoClicked(Photo photo) ;
}

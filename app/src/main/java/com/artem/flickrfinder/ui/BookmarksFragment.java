package com.artem.flickrfinder.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import com.artem.flickrfinder.R;
import com.artem.flickrfinder.rest.Photo;
import com.artem.flickrfinder.viewmodels.BookmarksViewModel;

public class BookmarksFragment extends PhotoListFragment {
    private BookmarksViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup parent, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(BookmarksViewModel.class);
        return super.onCreateView(inflater, parent, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        emptyText.setText(R.string.no_bookmarks);
        fab.setVisibility(View.GONE);

        viewModel.getPhotos().observe(this, photos -> {
            if (photos.isEmpty()) {
                //No photos -> show welcoming text
                emptyText.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                emptyText.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter.setData(photos);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onPhotoClicked(Photo photo) {
        Intent intent = FullscreenPhotoActivity.Companion.newInstanceWithId(getContext(), photo.getId(), photo.getTitle());
        startActivity(intent);
        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}

package com.artem.flickrfinder;

import android.app.Application;
import com.artem.flickrfinder.di.ContextModule;
import com.artem.flickrfinder.di.DaggerFlickrComponent;
import com.artem.flickrfinder.di.FlickrComponent;

public class FlickrFinderApplication extends Application {
    private static FlickrComponent component;

    public static FlickrComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //For simplicity, I will just store the three main dependencies in Application's component
        //A better approach would be injecting these at Activity/Fragment/ViewModel level, but that comes with increased
        //complexity of the app.
        component = DaggerFlickrComponent.builder()
            .contextModule(new ContextModule(getApplicationContext()))
            .build();
    }
}
